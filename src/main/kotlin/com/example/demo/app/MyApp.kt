package com.example.demo.app

import com.example.demo.view.MainView
import tornadofx.App

class MyApp : App() {
    override val primaryView = MainView::class
}

