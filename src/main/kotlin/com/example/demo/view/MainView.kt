package com.example.demo.view

import com.google.gson.Gson
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.TextField
import tornadofx.*
import java.io.File
import java.io.FileNotFoundException

class MainView : View("Instance Switcher") {
    private var instancesList = emptyList<Instance>().toMutableList().observable()

    private var editedInstance: Instance? = null

    var formInstanceName = textfield("6.5.0 without ZG")
    var formZgVersion = textfield("6.5.0")
    var formEnv = textfield("local")
    var formSkipAuthor = textfield("false")
    var formSkipPublish = textfield("true")
    var formSkipZenGarden = textfield("false")
    var formSkipHotfixes = textfield("true")
    var formSkipSampleExtensions = textfield("true")
    var formWorkflows = textfield("")
    var formInstallDir = textfield("D:\\AEM\\6.5.0")
    var formInstallErrorPage = textfield("false")

    var installerPath: TextField by singleAssign()
    var gradleInstallerPathHBox = hbox {
        label("zgcq-gradle-installer path")
        installerPath = textfield() {
            useMaxWidth = true
        }
    }

    var instanceListPath: TextField by singleAssign()
    var instanceListHBox = hbox {
        label("instances list path")
        instanceListPath = textfield() {
            useMaxWidth = true
        }
        instanceListPath.text = "config"
    }

    val selectedReading = SimpleObjectProperty<Instance>()
    var listViewRef = listview(instancesList) {
        bindSelected(selectedReading)
        cellFormat {
            text = "${it.name}"
        }
        prefHeight = 20.0
    }

    init {
        parsePreferences(readPreferences())
    }

    override fun onUndock() {
        savePreferences(getPreferencesFromView())
        super.onUndock()
    }

    override val root = hbox {
        this.add(listViewRef)
        vbox {
            useMaxWidth = true
            this.add(gradleInstallerPathHBox)
            this.add(instanceListHBox)

            hbox {
                button("Save instances list") {
                    setOnAction {
                        writeInstancesToFile(instanceListPath.getText(), instancesList)
                    }
                }
                button("Read instances list") {
                    setOnAction {
                        readInstanceList(instanceListPath.getText())
                    }
                }
            }
            hbox {
                button("Add").setOnAction { addInstance() }
                button("Edit").setOnAction { editInstance() }
                button("Save").setOnAction { saveEditedInstance() }
                button("Remove") {
                    setOnAction {
                        instancesList.removeAt(instancesList.indexOf(selectedReading.get()))
                        listViewRef.refresh()
                    }
                }
            }
            hbox {
                button("Modify props") {
                    setOnAction {
                        if (selectedReading.get() == null) {
                            alert(
                                Alert.AlertType.INFORMATION,
                                "You need to select instance from list",
                                "",
                                ButtonType.OK
                            )
                        } else {
                            if (modifyProps(installerPath.getText(), selectedReading.get())) {
                                alert(
                                    Alert.AlertType.INFORMATION,
                                    "Props has been modified",
                                    selectedReading.get().name,
                                    ButtonType.OK
                                )

                            }

                        }
                    }
                }
//                button("Switch instances") {
//                    setOnAction {
//                        alert(
//                            Alert.AlertType.INFORMATION,
//                            "Switching instance",
//                            selectedReading.get().name,
//                            ButtonType.OK
//                        )
//                        switchInstance(selectedReading.get())
//                    }
//                }
            }

            form {
                fieldset {
                    field("Instance name") {
                        this.add(formInstanceName)
                    }
                }
                fieldset {
                    field("Zen_Garden_version") {
                        this.add(formZgVersion)
                    }
                }
                fieldset {
                    field("Environment") {
                        this.add(formEnv)
                    }
                }
                fieldset {
                    field("Skip_author") {
                        this.add(formSkipAuthor)
                    }
                }
                fieldset {
                    field("Skip_publish") {
                        this.add(formSkipPublish)
                    }
                }
                fieldset {
                    field("Skip_Zen_Garden") {
                        this.add(formSkipZenGarden)
                    }
                }
                fieldset {
                    field("Skip_hotfixes") {
                        this.add(formSkipHotfixes)
                    }
                }
                fieldset {
                    field("Skip_sample_extensions") {
                        this.add(formSkipSampleExtensions)
                    }
                }
                fieldset {
                    field("Workflows") {
                        this.add(formWorkflows)
                    }
                }
                fieldset {
                    field("Install_dir") {
                        this.add(formInstallDir)
                    }
                }
                fieldset {
                    field("Install 500 Error Page") {
                        this.add(formInstallErrorPage)
                    }
                }
            }
        }
    }

    private fun saveEditedInstance() {
        if (editedInstance != null) {
            editedInstance!!.name = formInstanceName.text
            editedInstance!!.zgVersion = formZgVersion.text
            editedInstance!!.zgEnv = formEnv.text
            editedInstance!!.zgSkipAuthor = formSkipAuthor.text
            editedInstance!!.zgSkipPublish = formSkipPublish.text
            editedInstance!!.zgSkipZg = formSkipZenGarden.text
            editedInstance!!.zgSkipHotfixes = formSkipHotfixes.text
            editedInstance!!.zgSkipSampleExtensions = formSkipSampleExtensions.text
            editedInstance!!.zgWorkflows = formWorkflows.text
            editedInstance!!.zgInstallDir = formInstallDir.text
            editedInstance!!.zgInstallErrorPage = formInstallErrorPage.text
            listViewRef.refresh()
        }
    }

    private fun editInstance() {
        var inst = selectedReading.get()
        if (inst != null) {
            editedInstance = inst

            formInstanceName.text = inst.name
            formZgVersion.text = inst.zgVersion
            formEnv.text = inst.zgEnv
            formSkipAuthor.text = inst.zgSkipAuthor
            formSkipPublish.text = inst.zgSkipPublish
            formSkipZenGarden.text = inst.zgSkipZg
            formSkipHotfixes.text = inst.zgSkipHotfixes
            formSkipSampleExtensions.text = inst.zgSkipSampleExtensions
            formWorkflows.text = inst.zgWorkflows
            formInstallDir.text = inst.zgInstallDir
            formInstallErrorPage.text = inst.zgInstallErrorPage
        }
    }

    private fun modifyProps(propsPath: String, instance: Instance): Boolean {
        var props: List<String>
        try {
            props = readFileAsLines(propsPath)
        } catch (e: FileNotFoundException) {
            alert(
                Alert.AlertType.INFORMATION,
                "Properties file cannot be found",
                "Path $propsPath",
                ButtonType.OK
            )
            return false
        }
        var newProps: MutableList<String> = ArrayList()
        props.onEach {
            var line = it
            if (it.startsWith("zg.version=")) line = "zg.version=" + instance.zgVersion
            if (it.startsWith("zg.env=")) line = "zg.env=" + instance.zgEnv
            if (it.startsWith("zg.skip.author=")) line = "zg.skip.author=" + instance.zgSkipAuthor
            if (it.startsWith("zg.skip.publish=")) line = "zg.skip.publish=" + instance.zgSkipPublish
            if (it.startsWith("zg.skip.zg=")) line = "zg.skip.zg=" + instance.zgSkipZg
            if (it.startsWith("zg.skip.hotfixes=")) line = "zg.skip.hotfixes=" + instance.zgSkipHotfixes
            if (it.startsWith("zg.skip.sample-extensions=")) line =
                "zg.skip.sample-extensions=" + instance.zgSkipSampleExtensions
            if (it.startsWith("zg.workflows=")) line = "zg.workflows=" + instance.zgWorkflows
            if (it.startsWith("zg.install.dir=")) line = "zg.install.dir=" + instance.zgInstallDir
            if (it.startsWith("zg.install500ErrorPage")) line = "zg.install500ErrorPage=" + instance.zgInstallErrorPage
            newProps.add(line)
        }
        writeFile(propsPath, newProps)
        return true
    }

    private fun switchInstance(instance: Instance) {

        File("temp.bat").writeText("pushd D:\\workspace\\zgcq-gradle-installer\ngradlew instanceDown")
        var processAemDown = Runtime.getRuntime().exec("temp.bat")
        processAemDown.waitFor()
        modifyProps("D:\\workspace\\zgcq-gradle-installer\\gradle.properties", instance)
        File("temp.bat").writeText("pushd D:\\workspace\\zgcq-gradle-installer\ngradlew instanceUp")
        Runtime.getRuntime().exec("temp.bat")
    }

    private fun readInstanceList(instancesListPath: String) {
        var instancesFromFile: ObservableList<Instance>? = null
        try {
            instancesFromFile = readJSONtoList(instancesListPath)
        } catch (e: FileNotFoundException) {
            alert(
                Alert.AlertType.INFORMATION,
                "Instances list file cannot be found",
                "Path $instancesListPath",
                ButtonType.OK
            )
        }
        if (instancesFromFile != null) {
            instancesList.clear()
            instancesList.addAll(instancesFromFile)
        }
    }

    private fun parsePreferences(prefs: List<String>) {
        prefs.forEach {
            if (it.startsWith("gradle_installer_path=")) installerPath.text = it.removePrefix("gradle_installer_path=")
            if (it.startsWith("instances_list_path=")) instanceListPath.text = it.removePrefix("instances_list_path=")
        }

    }

    private fun getPreferencesFromView(): List<String> {
        return listOf(
            "gradle_installer_path=" + installerPath.text,
            "instances_list_path=" + instanceListPath.text
        )
    }

    private fun addInstance() {
        var instance = Instance(
            formInstanceName.text,
            formZgVersion.text,
            formEnv.text,
            formSkipAuthor.text,
            formSkipPublish.text,
            formSkipZenGarden.text,
            formSkipHotfixes.text,
            formSkipSampleExtensions.text,
            formWorkflows.text,
            formInstallDir.text,
            formInstallErrorPage.text
        )
        instancesList.add(instance)
    }
}

fun readJSONtoList(fileName: String): ObservableList<Instance> {
    var gson = Gson()
    val instancesList = mutableListOf<Instance>()
    val file = File(fileName)
    if (file.exists()) {
        file.forEachLine {
            instancesList.add(gson.fromJson(it, Instance::class.java))
        }
    } else {
        throw FileNotFoundException("Cannot find file $fileName")
    }
    return instancesList.observable()
}

private fun writeInstancesToFile(fileName: String, lines: List<Instance>) {
    val myFile = File(fileName)
    val gson = Gson()
    myFile.printWriter().use { out ->
        lines.forEach { out.println(gson.toJson(it)) }
    }
}

private fun writeFile(fileName: String, inputList: List<String>) {
    val file = File(fileName)
    file.printWriter().use { out ->
        inputList.forEach { out.println(it) }
    }
}

private fun readFileAsLines(fileName: String): List<String> {
    val file = File(fileName)
    if (file.exists()) {
        return file.useLines {
            it.toList()
        }
    } else {
        throw FileNotFoundException("Cannot find file $fileName")
    }

}

private fun readPreferences(): List<String> {
    val os = System.getProperty("os.name")
    var prefsFolder: File? = null
    var prefsFile: File? = null
    if (os.startsWith("Windows")) {
        val appDataPath = System.getenv("AppData")
        prefsFolder = File(appDataPath + "\\instance-switcher")
        prefsFile = File(prefsFolder.path + "\\preferences.txt")

    } else if (os.startsWith("Mac")) {
        prefsFolder = File("config")
        prefsFile = File(prefsFolder.path + "/preferences.txt")
    }
    if (prefsFolder != null && prefsFile != null) {
        if (prefsFolder.exists() && prefsFile.exists()) {
            return prefsFile.useLines { it.toList() }
        }
    }
    return emptyList()
}


private fun savePreferences(prefs: List<String>) {
    val os = System.getProperty("os.name").toLowerCase()
    var prefsFolder: File? = null
    var prefsFile: File? = null
    if (os.startsWith("windows")) {
        val appDataPath = System.getenv("AppData")
        prefsFolder = File(appDataPath + "\\instance-switcher")
        prefsFile = File(prefsFolder.path + "\\preferences.txt")


    } else if (os.startsWith("mac")) {
        prefsFolder = File("config")
        prefsFile = File(prefsFolder.path + "/preferences.txt")
    }

    if (prefsFolder != null && prefsFile != null) {
        if (!prefsFolder.exists()) {
            prefsFolder.mkdir()
        }
        if (!prefsFile.exists()) {
            prefsFile.createNewFile()
        }
        if (prefsFolder.exists() && prefsFile.exists()) {
            prefsFile.printWriter().use { out ->
                prefs.forEach {
                    out.println(it)
                }
            }
        }
    }
}

class Instance(
    name: String? = "Instance",
    zgVersion: String? = "6.5.0.0",
    zgEnv: String? = "local",
    zgSkipAuthor: String? = "false",
    zgSkipPublish: String? = "true",
    zgSkipZg: String? = "false",
    zgSkipHotfixes: String? = "true",
    zgSkipSampleExtensions: String? = "true",
    zgWorkflows: String? = "",
    zgInstallDir: String? = "D:\\AEM\\6.5.0 without ZG",
    zgInstallErrorPage: String? = "false"
) {
    var name = "$name"
    var zgVersion = "$zgVersion"
    var zgEnv = "$zgEnv"
    var zgSkipAuthor = "$zgSkipAuthor"
    var zgSkipPublish = "$zgSkipPublish"
    var zgSkipZg = "$zgSkipZg"
    var zgSkipHotfixes = "$zgSkipHotfixes"
    var zgSkipSampleExtensions = "$zgSkipSampleExtensions"
    var zgWorkflows = "$zgWorkflows"
    var zgInstallDir = "$zgInstallDir"
    var zgInstallErrorPage = "$zgInstallErrorPage"
}